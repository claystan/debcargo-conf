Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: procfs
Upstream-Contact: Andrew Chin <achin@eminence32.net>
Source: https://github.com/eminence/procfs

Files: *
Copyright: 2015 Andrew Chin <achin@eminence32.net>
License: MIT or Apache-2.0

Files: src/sys/kernel/random.rs
Copyright: 1994, 1995 by Daniel Quinlan (quinlan@yggdrasil.com)
  2002-2008,2017 Michael Kerrisk <mtk.manpages@gmail.com>
  with networking additions from Alan Cox (A.Cox@swansea.ac.uk)
  and scsi additions from Michael Neuffer (neuffer@mail.uni-mainz.de)
  and sysctl additions from Andries Brouwer (aeb@cwi.nl)
  and System V IPC (as well as various other) additions from
  Michael Kerrisk <mtk.manpages@gmail.com>
License: GFDL-1.2+

Files: debian/*
Copyright:
 2022 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2022 Joshua Peisach <itzswirlz2020@outlook.com>
License: MIT or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GFDL-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
 Texts.
 .
 On Debian GNU/Linux systems, the complete text of the GNU Free
 Documentation License can be found in
 `/usr/share/common-licenses/GFDL-1.2'.
