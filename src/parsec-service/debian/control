Source: rust-parsec-service
Section: rust
Priority: optional
Build-Depends: debhelper (>= 13),
 dh-cargo (>= 25),
 cargo:native,
 rustc:native,
 libstd-rust-dev,
 librust-anyhow-1+default-dev (>= 1.0.38-~~),
 librust-base64-0.13+default-dev,
 librust-bincode-1+default-dev (>= 1.3.1-~~),
 librust-derivative-2+default-dev (>= 2.1.0-~~),
 librust-env-logger-0.9+default-dev,
 librust-libc-0.2+default-dev (>= 0.2.86-~~),
 librust-log-0.4+default-dev (>= 0.4.14-~~),
 librust-log-0.4+serde-dev (>= 0.4.14-~~),
 librust-num-traits-0.2+default-dev (>= 0.2.14-~~),
 librust-parsec-interface-0.27+default-dev,
 librust-psa-crypto-0.9+operations-dev (>= 0.9.2-~~),
 librust-rusqlite-0.28+default-dev,
 librust-sd-notify-0.4+default-dev,
 librust-serde-1+default-dev (>= 1.0.123-~~),
 librust-serde-1+derive-dev (>= 1.0.123-~~),
 librust-signal-hook-0.3+default-dev (>= 0.3.4-~~),
 librust-structopt-0.3+default-dev (>= 0.3.21-~~),
 librust-threadpool-1+default-dev (>= 1.8.1-~~),
 librust-toml-0.5+default-dev (>= 0.5.8-~~),
 librust-users-0.11+default-dev,
 librust-uuid-1+default-dev (>= 1.2.0-~~),
 librust-zeroize-1+default-dev (>= 1.2.0-~~),
 librust-zeroize-1+zeroize-derive-dev (>= 1.2.0-~~)
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Emanuele Rocca <ema@debian.org>
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/parsec-service]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/parsec-service
X-Cargo-Crate: parsec-service
Rules-Requires-Root: no

Package: parsec-service
Architecture: any
Multi-Arch: allowed
Section: utils
Depends:
 adduser,
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
XB-X-Cargo-Built-Using: ${cargo:X-Cargo-Built-Using}
Description: Abstraction layer for secure storage and operations
 Parsec is an abstraction layer that can be used to interact with
 hardware-backed security facilities such as the Hardware Security Module (HSM),
 the Trusted Platform Module (TPM), as well as firmware-backed and isolated
 software services.
 .
 The core component of Parsec is the security service, provided by this package.
 The service is a background process that runs on the host platform and provides
 connectivity with the secure facilities of that host, exposing a
 platform-neutral API that can be consumed into different programming languages
 using a client library. For a client library implemented in Rust see the
 package librust-parsec-interface-dev.
