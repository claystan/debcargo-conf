From 409de55a56bc21daf1c8cdb920753763325c83ac Mon Sep 17 00:00:00 2001
From: James McCoy <jamessan@jamessan.com>
Date: Sun, 30 Oct 2022 17:02:18 -0400
Subject: [PATCH 1/2] Update to notify v5 via notify_debouncer_mini

v5 notify moved the debounced API into its own crate,
notify_debouncer_mini.  The debounced API doesn't provide details on the
type of event that happened, just that a list of events or errors
happened.  Therefore, trigger reload on any event for a matching path.

Disable default features for notify_debouncer_mini since std::sync::mpsc
is being used, so there's no need to pull in crossbeam-channel.
---
 Cargo.toml            |  2 +-
 src/config/monitor.rs | 35 ++++++++-------
 3 files changed, 78 insertions(+), 37 deletions(-)

--- a/Cargo.toml
+++ b/Cargo.toml
@@ -73,8 +73,9 @@ features = [
     "serde",
 ]
 
-[dependencies.notify]
-version = "4"
+[dependencies.notify-debouncer-mini]
+version = "0.2.1"
+default-features = false
 
 [dependencies.once_cell]
 version = "1.12"
--- a/src/config/monitor.rs
+++ b/src/config/monitor.rs
@@ -4,7 +4,8 @@ use std::sync::mpsc;
 
 use glutin::event_loop::EventLoopProxy;
 use log::{debug, error};
-use notify::{watcher, DebouncedEvent, RecursiveMode, Watcher};
+use notify_debouncer_mini::new_debouncer;
+use notify_debouncer_mini::notify::RecursiveMode;
 
 use alacritty_terminal::thread;
 
@@ -40,8 +41,8 @@ pub fn watch(mut paths: Vec<PathBuf>, event_proxy: EventLoopProxy<Event>) {
 
     // The Duration argument is a debouncing period.
     let (tx, rx) = mpsc::channel();
-    let mut watcher = match watcher(tx, DEBOUNCE_DELAY) {
-        Ok(watcher) => watcher,
+    let mut debouncer = match new_debouncer(DEBOUNCE_DELAY, None, tx) {
+        Ok(debouncer) => debouncer,
         Err(err) => {
             error!("Unable to watch config file: {}", err);
             return;
@@ -61,6 +62,7 @@ pub fn watch(mut paths: Vec<PathBuf>, event_proxy: EventLoopProxy<Event>) {
         parents.sort_unstable();
         parents.dedup();
 
+        let watcher = debouncer.watcher();
         // Watch all configuration file directories.
         for parent in &parents {
             if let Err(err) = watcher.watch(&parent, RecursiveMode::NonRecursive) {
@@ -69,26 +71,27 @@ pub fn watch(mut paths: Vec<PathBuf>, event_proxy: EventLoopProxy<Event>) {
         }
 
         loop {
-            let event = match rx.recv() {
-                Ok(event) => event,
+            let res = match rx.recv() {
+                Ok(res) => res,
                 Err(err) => {
                     debug!("Config watcher channel dropped unexpectedly: {}", err);
                     break;
                 },
             };
 
-            match event {
-                DebouncedEvent::Rename(_, path)
-                | DebouncedEvent::Write(path)
-                | DebouncedEvent::Create(path)
-                | DebouncedEvent::Chmod(path)
-                    if paths.contains(&path) =>
-                {
-                    // Always reload the primary configuration file.
-                    let event = Event::new(EventType::ConfigReload(paths[0].clone()), None);
-                    let _ = event_proxy.send_event(event);
+            match res {
+                Ok(events) => {
+                    if events.iter().any(|e| paths.contains(&e.path)) {
+                        // Always reload the primary configuration file.
+                        let event = Event::new(EventType::ConfigReload(paths[0].clone()), None);
+                        let _ = event_proxy.send_event(event);
+                    }
+                },
+                Err(errors) => {
+                    for err in errors {
+                        debug!("Config watcher notify error: {}", err);
+                    }
                 },
-                _ => (),
             }
         }
     });

From 1cdc355916246aca3515b7cc2cbd9b4858aab60f Mon Sep 17 00:00:00 2001
From: Christian Duerr <contact@christianduerr.com>
Date: Tue, 15 Nov 2022 13:53:41 +0100
Subject: [PATCH 2/2] Simplify error handling

---
 src/config/monitor.rs | 25 ++++++++++---------------
 1 file changed, 10 insertions(+), 15 deletions(-)

--- a/src/config/monitor.rs
+++ b/src/config/monitor.rs
@@ -71,27 +71,22 @@ pub fn watch(mut paths: Vec<PathBuf>, event_proxy: EventLoopProxy<Event>) {
         }
 
         loop {
-            let res = match rx.recv() {
-                Ok(res) => res,
+            let events = match rx.recv() {
+                Ok(Ok(events)) => events,
+                Ok(Err(err)) => {
+                    debug!("Config watcher errors: {:?}", err);
+                    continue;
+                },
                 Err(err) => {
                     debug!("Config watcher channel dropped unexpectedly: {}", err);
                     break;
                 },
             };
 
-            match res {
-                Ok(events) => {
-                    if events.iter().any(|e| paths.contains(&e.path)) {
-                        // Always reload the primary configuration file.
-                        let event = Event::new(EventType::ConfigReload(paths[0].clone()), None);
-                        let _ = event_proxy.send_event(event);
-                    }
-                },
-                Err(errors) => {
-                    for err in errors {
-                        debug!("Config watcher notify error: {}", err);
-                    }
-                },
+            if events.iter().any(|e| paths.contains(&e.path)) {
+                // Always reload the primary configuration file.
+                let event = Event::new(EventType::ConfigReload(paths[0].clone()), None);
+                let _ = event_proxy.send_event(event);
             }
         }
     });
