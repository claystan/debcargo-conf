Source: rust-sha3-0.9
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-block-buffer-0.9+block-padding-dev <!nocheck>,
 librust-block-buffer-0.9+default-dev <!nocheck>,
 librust-digest-0.9+default-dev <!nocheck>,
 librust-digest-0.9+std-dev <!nocheck>,
 librust-keccak-0.1+default-dev <!nocheck>,
 librust-opaque-debug-0.3+default-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Jochen Sprickerhof <jspricke@debian.org>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/sha3-0.9]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/sha3-0.9
Rules-Requires-Root: no

Package: librust-sha3-0.9-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-block-buffer-0.9+block-padding-dev,
 librust-block-buffer-0.9+default-dev,
 librust-digest-0.9+default-dev,
 librust-keccak-0.1+default-dev,
 librust-opaque-debug-0.3+default-dev
Recommends:
 librust-sha3-0.9+std-dev (= ${binary:Version})
Provides:
 librust-sha3-dev (= ${binary:Version}),
 librust-sha3-0-dev (= ${binary:Version}),
 librust-sha3-0.9.1-dev (= ${binary:Version})
Replaces: librust-sha3-dev (<< 0.9.2)
Breaks: librust-sha3-dev (<< 0.9.2)
Description: SHA-3 (Keccak) hash function - Rust source code
 This package contains the source for the Rust sha3 crate, packaged by debcargo
 for use with cargo and dh-cargo.

Package: librust-sha3-0.9+std-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-sha3-0.9-dev (= ${binary:Version}),
 librust-digest-0.9+std-dev
Provides:
 librust-sha3+std-dev (= ${binary:Version}),
 librust-sha3+default-dev (= ${binary:Version}),
 librust-sha3-0+std-dev (= ${binary:Version}),
 librust-sha3-0+default-dev (= ${binary:Version}),
 librust-sha3-0.9+default-dev (= ${binary:Version}),
 librust-sha3-0.9.1+std-dev (= ${binary:Version}),
 librust-sha3-0.9.1+default-dev (= ${binary:Version})
Description: SHA-3 (Keccak) hash function - feature "std" and 1 more
 This metapackage enables feature "std" for the Rust sha3 crate, by pulling in
 any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "default" feature.
